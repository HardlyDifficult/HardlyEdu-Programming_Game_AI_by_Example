﻿using System;
using UnityEngine;

public class Miner : BaseGameEntity {
    public static Miner instance;

    public StateMachine<Miner> stateMachine;

    public int goldOnPerson;
    public int goldInBank;
    public int fatigue;

    protected void Start() {
        instance = this;
        stateMachine = new StateMachine<Miner>(this, EnterMineAndDigForNugget.instance, MinerGlobalState.instance);
    }
  
    internal bool PocketsFull() {
        return goldOnPerson > 100;
    }

    protected void FixedUpdate() {
        stateMachine.Execute();
    }

    public override void HandleMessage(Telegram gram) {
        stateMachine.HandleMessage(gram);
    }
}

