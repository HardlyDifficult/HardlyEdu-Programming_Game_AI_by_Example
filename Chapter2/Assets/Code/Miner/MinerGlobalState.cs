﻿using System;
using UnityEngine;

public class MinerGlobalState : State<Miner> {
    public static MinerGlobalState instance = new MinerGlobalState();
    private MinerGlobalState() {
    }

    public override GameObject target {
        get {
            return null;
        }
    }

    public override bool HandleMessage(Miner entity, Telegram gram) {
        return false;
    }
    public override void Enter(Miner entity) {
    }

    public override void Execute(Miner entity) {
    }

    public override void Exit(Miner entity) {
    }
}

