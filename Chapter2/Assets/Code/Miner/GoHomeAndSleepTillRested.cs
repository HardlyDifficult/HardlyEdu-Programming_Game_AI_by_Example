﻿using System;
using UnityEngine;

public class GoHomeAndSleepTillRested : State<Miner> {
    public static GoHomeAndSleepTillRested instance = new GoHomeAndSleepTillRested();
    private GoHomeAndSleepTillRested() {
    }

    public override GameObject target {
        get {
            return Directory.instance.home;
        }
    }

    public override void Enter(Miner entity) {
        MessageDispatcher.instance.DispatchMessage(TimeSpan.Zero, entity, Elsa.instance, (int)MessageType.HiHoneyImHome); 
    }

    public override bool HandleMessage(Miner entity, Telegram gram) {
        if(gram.msg == (int)MessageType.StewReady) {
            entity.stateMachine.TravelTo(MinerEat.instance);
            return true;
        }
        return false;
    }

    public override void Execute(Miner entity) {
        if(UnityEngine.Random.Range(0f, 1f) < .001f) {
            entity.stateMachine.TravelTo(EnterMineAndDigForNugget.instance);
        }
    }

    public override void Exit(Miner entity) {
    }
}

