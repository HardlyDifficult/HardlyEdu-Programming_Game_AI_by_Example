﻿using System;
using UnityEngine;

public class VisitBandAndDepositGold : State<Miner> {
    public static VisitBandAndDepositGold instance = new VisitBandAndDepositGold();
    private VisitBandAndDepositGold() {
    }

    public override GameObject target {
        get {
            return Directory.instance.bank;
        }
    }

    public override void Enter(Miner entity) {
        entity.goldInBank += entity.goldOnPerson;
        entity.goldOnPerson = 0;
    }

    public override bool HandleMessage(Miner entity, Telegram gram) {
        return false;
    }

    public override void Execute(Miner entity) {
        if(UnityEngine.Random.Range(0f, 1f) < .1f) {
            entity.stateMachine.TravelTo(GoHomeAndSleepTillRested.instance);
        }
    }

    public override void Exit(Miner entity) {
    }
}

