﻿using System;
using UnityEngine;

public class EnterMineAndDigForNugget : State<Miner> {
    public static EnterMineAndDigForNugget instance = new EnterMineAndDigForNugget();
    private EnterMineAndDigForNugget() {
    }

    public override GameObject target {
        get {
            return Directory.instance.mine;
        }
    }

    public override bool HandleMessage(Miner entity, Telegram gram) {
        return false;
    }
    public override void Enter(Miner entity) {
    }

    public override void Execute(Miner entity) {
        entity.goldOnPerson++;
        entity.fatigue++;
        if(entity.PocketsFull()) {
            entity.stateMachine.TravelTo(VisitBandAndDepositGold.instance);
        }
    }

    public override void Exit(Miner entity) {
    }
}

