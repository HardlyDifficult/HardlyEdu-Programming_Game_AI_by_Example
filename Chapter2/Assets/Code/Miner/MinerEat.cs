﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MinerEat : State<Miner> {
    public static MinerEat instance = new MinerEat();
    private MinerEat() {
    }

    public override GameObject target {
        get {
            return Directory.instance.kitchen;
        }
    }

    public override void Enter(Miner entity) {
    }

    public override void Execute(Miner entity) {
        if(UnityEngine.Random.Range(0f, 1f) < .01) {
            entity.stateMachine.TravelTo(GoHomeAndSleepTillRested.instance);
        }
    }

    public override void Exit(Miner entity) {
    }

    public override bool HandleMessage(Miner entity, Telegram gram) {
        return false;
    }
}

