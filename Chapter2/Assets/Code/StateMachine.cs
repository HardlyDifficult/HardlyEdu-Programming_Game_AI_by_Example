﻿using System;
using UnityEngine;

public class StateMachine<EntityType> where EntityType : BaseGameEntity {
    EntityType owner;
    State<EntityType> currentState;
    State<EntityType> globalState;
    State<EntityType> previousState;
    internal GameObject target;
    State<EntityType> destinationState;

    public StateMachine(EntityType owner, State<EntityType> currentState, State<EntityType> globalState) {
        this.owner = owner;
        TravelTo(currentState);
        this.globalState = globalState;
    }

    public void ChangeState(State<EntityType> newState) {
        MonoBehaviour.print(newState.GetType());
        if(currentState != null) {
            currentState.Exit(owner);
        }
        currentState = newState;
        currentState.Enter(owner);
    }


    public void HandleMessage(Telegram gram) {
        if(currentState.HandleMessage(owner, gram)) {
            return;
        }
        if(globalState.HandleMessage(owner, gram)) {
            return;
        }
    }

    public void TravelTo(State<EntityType> destinationState) {
        target = destinationState.target;
        this.destinationState = destinationState;
        ChangeState(new Travel<EntityType>(destinationState));
    }

    public void RevertToPreviousState() {
        ChangeState(previousState);
    }

    public void Execute() {
        if(globalState != null) {
            globalState.Execute(owner);
        }
        if(currentState != null) {
            currentState.Execute(owner);
        }
        if(target != null) {
            owner.transform.position = Vector3.MoveTowards(owner.transform.position,
                target.transform.position,
                owner.moveSpeed * Time.deltaTime);
            if(owner.transform.position == target.transform.position) {
                target = null;
                ChangeState(destinationState);
            }
        }
    }
}

