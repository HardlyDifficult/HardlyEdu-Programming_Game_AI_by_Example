﻿using System;
using UnityEngine;

internal class Travel<EntityType> : State<EntityType> where EntityType : BaseGameEntity {
    private State<EntityType> newState;

    public override GameObject target {
        get {
            return null;
        }
    }

    public Travel(State<EntityType> newState) {
        this.newState = newState;
    }

    public override bool HandleMessage(EntityType entity, Telegram gram) {
        return false;
    }
    public override void Enter(EntityType entity) {
    }

    public override void Execute(EntityType entity) {
    }

    public override void Exit(EntityType entity) {
    }
}