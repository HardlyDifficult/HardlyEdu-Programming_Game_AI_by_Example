﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class MessageDispatcher {
    public static MessageDispatcher instance = new MessageDispatcher();
    private MessageDispatcher() {
    }

    SortedList<DateTime, Telegram> telegrams = new SortedList<DateTime, Telegram>();
    // TODO dispatch delayed messages

    public void DispatchMessage(TimeSpan delay, BaseGameEntity sender, BaseGameEntity receiver, int message) {
        var telegram = new Telegram(sender, receiver, message, DateTime.Now + delay);
        if(delay == TimeSpan.Zero) {
            Discharge(telegram);
        } else {
            telegrams.Add(telegram.dispatchTime, telegram);
        }
    }

    private void Discharge(Telegram telegram) {
        telegram.receiver.HandleMessage(telegram);
    }
}

