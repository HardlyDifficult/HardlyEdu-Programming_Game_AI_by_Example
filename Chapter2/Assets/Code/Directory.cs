﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Directory : MonoBehaviour {
    public GameObject mine;
    public GameObject home;
    public GameObject bank;
    public GameObject outhouse;
    public GameObject kitchen;


    public static Directory instance;


	// Use this for initialization
	void Awake () {
        instance = this;
	}	
	
}
