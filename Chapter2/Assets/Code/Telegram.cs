﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class Telegram {
    public readonly BaseGameEntity sender, receiver;
   public readonly int msg;
    public readonly DateTime dispatchTime;

    public Telegram(BaseGameEntity sender, BaseGameEntity receiver, int msg, DateTime dispatchTime) {
        this.sender = sender;
        this.receiver = receiver;
        this.msg = msg;
        this.dispatchTime = dispatchTime;
    }

}

