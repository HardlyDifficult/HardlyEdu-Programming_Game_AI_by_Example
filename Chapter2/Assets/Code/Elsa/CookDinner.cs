﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CookDinner : State<Elsa> {
    public static CookDinner instance = new CookDinner();
    private CookDinner() {
    }

    public override GameObject target {
        get {
            return Directory.instance.kitchen;
        }
    }

    public override void Enter(Elsa entity) {
    }

    public override void Execute(Elsa entity) {
        if(UnityEngine.Random.Range(0f, 1f) < .01f) {
            MessageDispatcher.instance.DispatchMessage(TimeSpan.Zero, entity, Miner.instance, (int)MessageType.StewReady);
            entity.stateMachine.TravelTo(ElsaEat.instance);
        }
    }

    public override void Exit(Elsa entity) {
        entity.lastDinner = DateTime.Now;
    }

    public override bool HandleMessage(Elsa entity, Telegram gram) {
        return false;
    }
}

