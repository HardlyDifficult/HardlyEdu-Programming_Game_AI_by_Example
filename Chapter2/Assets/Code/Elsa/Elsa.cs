﻿using System;
using UnityEngine;

public class Elsa : BaseGameEntity {
    public static Elsa instance;

    public StateMachine<Elsa> stateMachine;
    public DateTime lastDinner;
    
    protected void Start() {
        instance = this;
        stateMachine = new StateMachine<Elsa>(this, DoHousework.instance, ElsaGlobalState.instance);
    }

   protected void FixedUpdate() {
        stateMachine.Execute();
    }

    public override void HandleMessage(Telegram gram) {
        stateMachine.HandleMessage(gram);
    }
}

