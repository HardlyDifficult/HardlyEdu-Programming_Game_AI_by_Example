﻿using System;
using UnityEngine;

public class ElsaEat : State<Elsa> {
    public static ElsaEat instance = new ElsaEat();
    private ElsaEat() {
    }

    public override GameObject target {
        get {
            return Directory.instance.kitchen;
        }
    }

    public override void Enter(Elsa entity) {
    }

    public override void Execute(Elsa entity) {
        if(UnityEngine.Random.Range(0f, 1f) < .001) {
            entity.stateMachine.TravelTo(DoHousework.instance);
        }
    }

    public override void Exit(Elsa entity) {
    }

    public override bool HandleMessage(Elsa entity, Telegram gram) {
        return false;
    }
}

