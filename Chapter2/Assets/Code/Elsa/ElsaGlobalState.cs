﻿using System;
using UnityEngine;

public class ElsaGlobalState : State<Elsa> {
    public static ElsaGlobalState instance = new ElsaGlobalState();
    private ElsaGlobalState() {
    }

    public override GameObject target {
        get {
            return null;
        }
    }

    public override bool HandleMessage(Elsa entity, Telegram gram) {
        if(gram.msg == (int)MessageType.HiHoneyImHome) {
            if(DateTime.Now - entity.lastDinner > TimeSpan.FromSeconds(20))
            entity.stateMachine.TravelTo(CookDinner.instance);
            return true;
        }
        return false;
    }

    public override void Enter(Elsa entity) {
    }

    public override void Execute(Elsa entity) {
    }

    public override void Exit(Elsa entity) {
    }
}

