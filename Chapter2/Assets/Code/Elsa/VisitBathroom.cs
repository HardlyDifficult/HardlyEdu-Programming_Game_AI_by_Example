﻿using System;
using UnityEngine;

public class VisitBathroom : State<Elsa> {
    public static VisitBathroom instance = new VisitBathroom();
    private VisitBathroom() {
    }

    public override GameObject target {
        get {
            return Directory.instance.outhouse;
        }
    }

    public override bool HandleMessage(Elsa entity, Telegram gram) {
        return false;
    }

    public override void Enter(Elsa entity) {
    }

    public override void Execute(Elsa entity) {
        if(UnityEngine.Random.Range(0f, 1f) < 0.01) {
            entity.stateMachine.TravelTo(DoHousework.instance);
        }
    }

    public override void Exit(Elsa entity) {
    }
}

