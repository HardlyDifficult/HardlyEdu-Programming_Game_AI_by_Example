﻿using System;
using UnityEngine;

public class DoHousework : State<Elsa> {
    public static DoHousework instance = new DoHousework();
    private DoHousework() {
    }

    public override GameObject target {
        get {
            return Directory.instance.home;
        }
    }

    public override bool HandleMessage(Elsa entity, Telegram gram) {
        return false;
    }

    public override void Enter(Elsa entity) {
    }

    public override void Execute(Elsa entity) {
        if(UnityEngine.Random.Range(0f, 1f) < 0.001) {
            entity.stateMachine.TravelTo(VisitBathroom.instance);
        }
    }

    public override void Exit(Elsa entity) {
    }
}