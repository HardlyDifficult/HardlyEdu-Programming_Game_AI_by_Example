﻿using System;
using UnityEngine;

public abstract class State<EntityType> where EntityType : BaseGameEntity {
    public abstract GameObject target {
        get;
    }

    public abstract void Enter(EntityType entity);
    public abstract void Execute(EntityType entity);
    public abstract void Exit(EntityType entity);
    public abstract bool HandleMessage(EntityType entity, Telegram gram);
}

