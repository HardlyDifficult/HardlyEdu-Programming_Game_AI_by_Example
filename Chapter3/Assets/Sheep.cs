﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheep : MonoBehaviour {
    public float maxSpeed = 0.01f;
    public float neighborRadius = 6f;
    List<Sheep> neighbors = new List<Sheep>();
    Vector3 velocity;
    public float mass = 1;

    public float separationWeight;
    //public float alignmentWeight;
    public float cohesionWeight;
    public float wanderWeight;
    Vector2 wanderTarget;
    public float wanderRadius;
    public float wanderJitter;
    public float wanderDistance;
    public float evadeWeight;

    Dog dog;

    protected void Start()
    {
        dog = GameObject.Find("Dog").GetComponent<Dog>();
    }

    protected void Update()
    {
        neighbors.Clear();
        FindNeighbors();
        var steeringForce = Vector3.zero;
        steeringForce += Evade(dog.transform) * evadeWeight;
        steeringForce += Separation() * separationWeight;
        //steeringForce += Alignment() * alignmentWeight;
        steeringForce += Cohesion() * cohesionWeight;
        steeringForce += Wander() * wanderWeight;
        steeringForce = new Vector3(steeringForce.x, 0, steeringForce.z);

        var acceleration = steeringForce / mass;
        velocity += acceleration * Time.deltaTime;
        velocity = Vector3.ClampMagnitude(velocity, maxSpeed);
        transform.position += velocity * Time.deltaTime;
        //transform.rotation = Quaternion.LookRotation(velocity);
    }

    private Vector3 Evade(Transform targetTransform)
    {
        var toTarget = targetTransform.position - transform.position;
        if(toTarget.magnitude > 12)
        {
            return Vector3.zero;
        }

        var lookAheadTime = toTarget.magnitude / (maxSpeed + dog.speed);

        return Flee(dog.transform.position + dog.velocity * lookAheadTime);
    }

    private Vector3 Flee(Vector3 target)
    {
        var desiredVelocity = (transform.position - target).normalized * maxSpeed;
        return desiredVelocity - velocity;
    }

    private Vector3 Wander()
    {
        wanderTarget += new Vector2(UnityEngine.Random.Range(0, wanderJitter), UnityEngine.Random.Range(0, wanderJitter));
        wanderTarget = wanderTarget.normalized * wanderRadius;
        var targetLocalSpace = wanderTarget + new Vector2(wanderDistance, 0);
        var target = transform.TransformPoint(targetLocalSpace);
        return target - transform.position;
    }

    private Vector3 Cohesion()
    {
        if(neighbors.Count == 0)
        {
            return Vector3.zero;
        }
        var centerOfMass = Vector3.zero;
        foreach(var neighbor in neighbors)
        {
            centerOfMass += neighbor.transform.position;
        }
        centerOfMass /= neighbors.Count;
        return Seek(centerOfMass);
    }

    private Vector3 Seek(Vector3 target)
    {
        var desiredVelocity = (target - transform.position).normalized * maxSpeed;
        return desiredVelocity - velocity;
    }

    private Vector3 Alignment()
    {
        if(neighbors.Count == 0)
        {
            return Vector3.zero;
        }

        var averageHeading = Vector3.zero;
        foreach(var neighbor in neighbors)
        {
            averageHeading += neighbor.transform.forward;
        }
        averageHeading /= neighbors.Count;
        return averageHeading - transform.forward;        
    }

    private Vector3 Separation()
    {
        var steeringForce = Vector3.zero;
        foreach(var neighbor in neighbors)
        {
            var toAgent = transform.position - neighbor.transform.position;
            steeringForce += toAgent.normalized / toAgent.magnitude;
        }
        return steeringForce;
    }

    private void FindNeighbors()
    {
        var sheepColliders = Physics.OverlapSphere(transform.position, neighborRadius, 
            LayerMask.GetMask(new[] { "Sheep" }));
        foreach(var sheepCollider in sheepColliders)
        {
            if(sheepCollider.gameObject != gameObject)
            {
                neighbors.Add(sheepCollider.GetComponent<Sheep>());
            }
        }
    }
}
