﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : MonoBehaviour {
    public float speed = 1;
    public Vector3 velocity;

	// Update is called once per frame
	void Update () {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        velocity = new Vector3(horizontal, 0, vertical) * speed * Time.deltaTime;
        transform.position += velocity;

    }
}
